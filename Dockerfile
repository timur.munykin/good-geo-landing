FROM nginx:alpine

COPY ssl/certificate.crt /etc/ssl/certs/
COPY ssl/certificate.key /etc/ssl/private/
COPY ssl/certificate_ca.crt /etc/ssl/certs/

COPY nginx.conf /etc/nginx/nginx.conf

COPY web /usr/share/nginx/html

EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]
